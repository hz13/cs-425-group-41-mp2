package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	"../labrpc"
)

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

type LogEntry struct {
	Command interface{} // The command for the state machine
	Term    int         // The term when entry was received by leader
}

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
	// Your data here (2A, 2B).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	// You may also need to add other state, as per your implementation.
	mu      sync.RWMutex        // Lock to protect shared access to this peer's state
	peers   []*labrpc.ClientEnd // RPC end points of all peers
	me      int                 // this peer's index into peers[]
	dead    int32               // set by Kill()
	applyCh chan ApplyMsg       // The communication channel to the state machine.

	// Persistent state on all servers
	currentTerm  int         // latest term server as seen, initialized to 0
	currentState string      // Current state of the server, can be either "Follower", "Candidate", or "Leader".
	votedFor     int         // candidateId that received vote in the current term, null value is -1
	log          []*LogEntry // the log entries that each server maintains
	resetTimeOut bool        // reset timeout value as changed in heartbeat RPC handler and requestVote RPC handler

	// Volatile state on all servers
	// commit index and lastApplied is BASE 1.
	commitIndex int // index of the highest log entry known to be commited, initialized to 0

	// Volatile state on leaders (reinitialized after election)
	nextIndex  []int // for each server, index of the next log entry to send to that server, initialized to leader last log index + 1
	matchIndex []int // for each server, index of highest log entry known to be replicated on server, initialized to 0

	// User added components.
	Args_out        []*RequestVoteReply
	Append_args_out []*AppendEntriesReply
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (2A).
	rf.mu.Lock()
	term = rf.currentTerm
	isleader = rf.currentState == "Leader"
	rf.mu.Unlock()
	return term, isleader
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term         int // candidate term
	CandidateId  int // candidate requesting vote
	LastLogIndex int // index of candidate's last log entry
	LastLogTerm  int // term of candidate's last log entry
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (2A).
	Term        int  // currentTerm, for candidate to update itself
	VoteGranted bool // true means cadidate received vote
}

//
// example AppendEntries RPC argument structure.
// field names must start with capital letters.
//
type AppendEntriesArgs struct {
	Term         int         // leader's term
	LeaderId     int         // leader's Id (sender's Id)
	PrevLogIndex int         // index of log entry immediately preceeding new ones, negative value indicates NO prev log!!
	PrevLogTerm  int         // term of log entry immediately preceding new ones
	LogEntries   []*LogEntry // empty for heartbeat, log entries to store, multiple log entries in slice.
	LeaderCommit int         // leader's commit index
	Type         string      // type of AEA, it can be either "Heartbeat", or "AppendEntries"
}

//
// example AppendEntries RPC reply structure.
// field names must start with capital letters.
//
type AppendEntriesReply struct {
	Term          int
	Success       bool
	IsHeartbeat   bool
	ConflictTerm  int
	ConflictIndex int
}

//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).
	// Read the fields in "args",
	// and accordingly assign the values for fields in "reply".
	if rf.killed() {
		return
	}
	rf.mu.Lock()
	if args.Term < rf.currentTerm {
		reply.Term = rf.currentTerm
		rf.mu.Unlock()
		reply.VoteGranted = false
		//fmt.Printf("Server %v replied false for candidate %v 's election, TERM\n", rf.me, args.CandidateId)
		return
	}
	if args.Term > rf.currentTerm {
		rf.currentTerm = args.Term
		rf.votedFor = -1
		rf.currentState = "Follower"
		//fmt.Printf("Server %v changed its term to %v and become follower\n", rf.me, rf.currentTerm)
	}
	lastTermV := 0
	lastIndexV := 0
	logUpToDate := true
	if len(rf.log) > 1 {
		lastTermV = rf.log[len(rf.log)-1].Term
		lastIndexV = len(rf.log) - 1
		logUpToDate = (lastTermV > args.LastLogTerm) || (lastTermV == args.LastLogTerm && lastIndexV > args.LastLogIndex)

	} else {
		logUpToDate = false
	}
	if (!logUpToDate) && (rf.votedFor == -1 || rf.votedFor == args.CandidateId) {
		rf.resetTimeOut = true
		reply.Term = rf.currentTerm
		reply.VoteGranted = true
		rf.votedFor = args.CandidateId
		// Also need to reset timeout.

		//fmt.Printf("Server %v replied true for candidate %v 's election\n", rf.me, args.CandidateId)
	} else {
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		//fmt.Printf("Server %v replied false for candidate %v 's election\n", rf.me, args.CandidateId)

	}
	rf.mu.Unlock()
	return
}

func (rf *Raft) findFirstIndexOfTerm(term int) int {
	for i := range rf.log {
		if i == 0 {
			continue
		}
		if rf.log[i].Term == term {
			return i
		}
	}
	return 0
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	if ok == true {
		rf.mu.Lock()
		rf.Args_out[server] = reply
		if reply.Term > rf.currentTerm {
			//fmt.Printf("Leader %v in term %v get reply with higher term %v and updates itself to Follower\n", rf.me, rf.currentTerm, reply.Term)
			rf.currentTerm = reply.Term
			rf.currentState = "Follower"
			rf.resetTimeOut = true
			rf.mu.Unlock()
		} else {
			rf.mu.Unlock()
		}
	}

	return ok
}

//
// example code for the AppendEntries handler.
//
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	// #TODO: 2A and 2B.
	// send heartbeat for now, TODO: add log entries for 2B
	if rf.killed() {
		return
	}
	reply.ConflictTerm = -1
	reply.ConflictIndex = 0
	rf.mu.Lock()
	//fmt.Printf("Server %v receives message with index %v, with args log length %v\n", rf.me, rf.commitIndex, len(args.LogEntries))

	isHeartbeat := len(args.LogEntries) == 0
	PrevTermNotExist := (args.PrevLogIndex < 1)
	PrevTermExistsAndValid := (len(rf.log) > args.PrevLogIndex) && (args.PrevLogIndex >= 1 && rf.log[args.PrevLogIndex].Term == args.PrevLogTerm)
	// isValid := !isHeartbeat && (LeaderHasNoLog || ValidTerm)
	//fmt.Println(args.PrevLogIndex, isValid, LeaderHasNoLog)
	reply.IsHeartbeat = isHeartbeat
	// Reply false if the argument term is smaller than the current term.
	if rf.currentTerm > args.Term {
		//fmt.Printf("%v replies false 1\n", rf.me)
		reply.Success = false
		reply.Term = rf.currentTerm
		rf.mu.Unlock()
		return
	} else if isHeartbeat {
		// We first check whether it corresponds to new leader command request.
		// We then check whether leader has one log or not.
		// If only one log, automatic success.
		// If more are available, we need to look whether terms are equal.
		// NOTE: if the new entry has been committed, and it is a heartbeat, reply false
		// OTHERWISE, reply true.
		// Reply true, and fix its own commit index if necessary.
		rf.resetTimeOut = true
		reply.Success = true
		reply.Term = rf.currentTerm
		rf.currentTerm = args.Term
		rf.currentState = "Follower"
		if !(PrevTermExistsAndValid || PrevTermNotExist) {
			//fmt.Printf("server %v PrevTermExistsAndValid %v, PrevTermNotExist %v, PrevLogIndex %v, reply false\n", rf.me, PrevTermExistsAndValid, PrevTermNotExist, args.PrevLogIndex)
			reply.Success = false
			if len(rf.log) <= args.PrevLogIndex {
				reply.ConflictTerm = rf.log[len(rf.log)-1].Term
				reply.ConflictIndex = rf.findFirstIndexOfTerm(reply.ConflictTerm)
			}
			rf.mu.Unlock()
			return
		}

	} else {
		// fmt.Printf("Server %v receives AppendEntries with prev index %v, with argslog length %v\n", rf.me, args.PrevLogIndex, len(args.LogEntries))
		// for j := range args.LogEntries {
		// 	fmt.Printf("%v %v ", args.LogEntries[j].Command, args.LogEntries[j].Term)
		// }
		// fmt.Printf("\n")
		// for index := range rf.log {
		// 	if index > 0 {
		// 		fmt.Printf("%v %v ", rf.log[index].Command, rf.log[index].Term)
		// 	}
		// }
		// fmt.Printf("\n")

		rf.currentTerm = args.Term
		rf.currentState = "Follower"
		reply.Term = rf.currentTerm
		rf.resetTimeOut = true

		if !(PrevTermExistsAndValid || PrevTermNotExist) {
			reply.Success = false
			if len(rf.log) <= args.PrevLogIndex {
				reply.ConflictTerm = rf.log[len(rf.log)-1].Term
				reply.ConflictIndex = rf.findFirstIndexOfTerm(reply.ConflictTerm)
			}
			rf.mu.Unlock()
			return
		}
		reply.Success = true
		// check whether the existing entry conflicts with a new one.
		NewLogIndexExistsLog := (len(rf.log) > (args.PrevLogIndex + 1))
		if NewLogIndexExistsLog {
			differingIndex := 0

			for i := args.PrevLogIndex + 1; i < len(rf.log); i++ {
				// TODO: Is this correct
				if len(args.LogEntries)-1 < i-args.PrevLogIndex-1 {
					//fmt.Println(">_<")
					reply.Success = false
					// if len(rf.log) <= args.PrevLogIndex {
					// 	reply.ConflictTerm = rf.log[len(rf.log)-1].Term
					// 	reply.ConflictIndex = rf.findFirstIndexOfTerm(reply.ConflictTerm)
					// }
					rf.mu.Unlock()
					return
				}
				if rf.log[i].Term != args.LogEntries[i-args.PrevLogIndex-1].Term {
					differingIndex = i
					break
				}
			}
			if differingIndex > 1 {
				// there is an element that differs, get rid of all entries following it.
				rf.log = rf.log[0:differingIndex]
				// put the new entries inside.
				for i := differingIndex - args.PrevLogIndex - 1; i < len(args.LogEntries); i++ {
					rf.log = append(rf.log, args.LogEntries[i])
				}
				// fmt.Println("A")
				// for index := range rf.log {
				// 	if index > 0 {
				// 		fmt.Printf("%v ", rf.log[index].Command)
				// 	}
				// }
				// fmt.Printf("\n")
			} else {
				expectedLength := len(rf.log)
				if expectedLength < args.PrevLogIndex+len(args.LogEntries)+1 {
					rf.log = append(rf.log, args.LogEntries[len(rf.log)-1-args.PrevLogIndex:]...)
				} else {
					// There might be extraneous entries, delete the extraneous entries.
					rf.log = rf.log[0 : args.PrevLogIndex+len(args.LogEntries)+1]
				}
				// fmt.Println("B")
				// for index := range rf.log {
				// 	if index > 0 {
				// 		fmt.Printf("%v ", rf.log[index].Command)
				// 	}

				// }
				// fmt.Printf("\n")

			}
		} else {
			// If index does not exist, then we append the entry at the end of the local log.
			if len(args.LogEntries) > 0 {
				//fmt.Printf("Server %v append %v\n", rf.me, args.LogEntries[0].Command)
				rf.log = append(rf.log, args.LogEntries...)
				// fmt.Println("C")
				// for index := range rf.log {
				// 	if index > 0 {
				// 		fmt.Printf("%v ", rf.log[index].Command)
				// 	}

				// }
				// fmt.Printf("\n")
			}

		}
	}
	// set commit index in either Heartbeat and AppendEntries
	if args.LeaderCommit > rf.commitIndex {
		oldCommitIndex := rf.commitIndex
		if args.LeaderCommit >= len(rf.log)-1 {
			rf.commitIndex = len(rf.log) - 1
		} else {
			rf.commitIndex = args.LeaderCommit
		}
		//fmt.Printf("Server %v COMMIT index %v\n", rf.me, rf.commitIndex)
		// send the commitedMessages to the applyCh
		for idx := oldCommitIndex + 1; idx <= rf.commitIndex; idx++ {
			committedMsg := new(ApplyMsg)
			committedMsg.Command = rf.log[idx].Command
			committedMsg.CommandIndex = idx
			committedMsg.CommandValid = true
			rf.applyCh <- *committedMsg
		}
	}
	rf.mu.Unlock()
	return
}

//
// example code to send AppendEntriesRPC to a server.
//
func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	//rf.mu.Lock()
	//fmt.Printf("Leader %v send message Type %v to server %v with next index %v, Leader Commit index %v\n", rf.me, args.Type, server, rf.nextIndex[server], args.LeaderCommit)
	//rf.mu.Unlock()
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	if rf.killed() {
		return ok
	}
	rf.mu.Lock()
	if ok == true {
		if reply.Term > rf.currentTerm {
			//fmt.Printf("Leader %v in term %v get reply with higher term %v and updates itself to Follower\n", rf.me, rf.currentTerm, reply.Term)
			rf.currentTerm = reply.Term
			rf.currentState = "Follower"
			rf.mu.Unlock()
			return ok
		}
		rf.Append_args_out[server] = reply
	} else {
		//fmt.Printf("Send message Type %v to server %v with next index %v FAILED\n", args.Type, server, rf.nextIndex[server])
		rf.mu.Unlock()
		return ok
	}
	// count := 0
	majority_number := len(rf.peers)/2 + 1

	if rf.currentTerm == rf.Append_args_out[server].Term {
		if rf.Append_args_out[server].Success == true {
			// Mission successful, We may update the nextIndex and the matchIndex for each server.
			// IMPORTANT!!! NOTICE. If AppendEntries is successful, we ASSUME that the follower now has the SAME log as the leader.
			// Based on this assumption, we assign the matchIndex and the nextIndex accordingly!!!
			if rf.nextIndex[server] <= args.PrevLogIndex+len(args.LogEntries)+1 {
				rf.nextIndex[server] = args.PrevLogIndex + len(args.LogEntries) + 1
				rf.matchIndex[server] = args.PrevLogIndex + len(args.LogEntries)
			}

			//fmt.Printf("With reply TRUE, Leader %v updates server %v next index to %v\n", rf.me, server, rf.nextIndex[server])
			// rf.nextIndex[server] = len(rf.log)
			// rf.matchIndex[server] = len(rf.log) - 1
			// fmt.Printf("%v %v\n", args.PrevLogIndex+len(args.LogEntries)+1, len(rf.log))
		} else {
			// Mission unsuccessful, pushback the next_index for this follower by 1.
			// Check whether nextIndex can be pushed back even further.
			if reply.ConflictIndex != 0 && reply.ConflictTerm != -1 && rf.nextIndex[server]-1 > reply.ConflictIndex+1 {
				//fmt.Printf("With reply -FALSE- from %v, Leader %v updates server %v next index from %v to %v\n", reply.IsHeartbeat, rf.me, server, rf.nextIndex[server], reply.ConflictIndex+1)
				rf.nextIndex[server] = reply.ConflictIndex + 1
				rf.matchIndex[server] = rf.nextIndex[server] - 1

			} else if rf.nextIndex[server] > 1 {
				// fmt.Println("Haha I screwed up")
				if rf.matchIndex[server]-2 > -1 {
					rf.matchIndex[server] -= 2
				} else if rf.matchIndex[server]-1 > -1 {
					rf.matchIndex[server] -= 1
				}
				if rf.nextIndex[server]-2 > 0 {
					rf.nextIndex[server] -= 2
				} else if rf.nextIndex[server]-1 > 0 {
					rf.nextIndex[server] -= 1
				}

				// if rf.nextIndex[server]-1 > -1 {
				// 	rf.nextIndex[server] -= 1
				// }
				// if rf.matchIndex[server]-1 > -1 {
				// 	rf.matchIndex[server] -= 1
				// }
				//fmt.Printf("With reply FALSE from %v, Leader %v updates server %v next index to %v\n", reply.IsHeartbeat, rf.me, server, rf.nextIndex[server])
			}
		}
	}
	canCommit := false
	candidateCommitIndex := 0
	match_count := 0
	for i := 5; i > 0; i -= 1 {
		candidateCommitIndex = rf.commitIndex + i
		match_count = 0
		if candidateCommitIndex > len(rf.log)-1 {
			continue
		}
		for ch_idx := range rf.peers {
			if rf.matchIndex[ch_idx] >= candidateCommitIndex {
				match_count += 1
			}
		}
		match_count += 1
		if rf.log[candidateCommitIndex].Term == rf.currentTerm && match_count >= majority_number {
			canCommit = true
			break
		}
	}
	//fmt.Printf("match count number is %v, canCommit = %v, originalCommmitIndex = %v\n", match_count, canCommit, rf.commitIndex)
	// if count >= majority_number {
	// 	// Majority of servers received message, can change commit index.
	// 	// All log messages are committed.
	// 	// Send commit messages to the tester.
	// 	oldCommitIndex := rf.commitIndex
	// 	rf.commitIndex = len(rf.log) - 1
	// 	//fmt.Printf("Leader %v old commit index %v, new commit index %v\n", rf.me, oldCommitIndex, rf.commitIndex)
	// 	for idx := oldCommitIndex + 1; idx < len(rf.log); idx++ {
	// 		committedMsg := new(ApplyMsg)
	// 		committedMsg.Command = rf.log[idx].Command
	// 		committedMsg.CommandIndex = idx
	// 		committedMsg.CommandValid = true
	// 		rf.applyCh <- *committedMsg
	// 		//fmt.Printf("Leader %v commits message index %v with %v votes\n", rf.me, rf.commitIndex, count)
	// 	}
	// }

	if canCommit {
		// Majority of servers received message, can change commit index.
		// All log messages are committed.
		// Send commit messages to the tester.
		oldCommitIndex := rf.commitIndex
		rf.commitIndex = candidateCommitIndex
		//fmt.Printf("Leader %v COMMIT, update old commit index %v to commit index %v\n", rf.me, oldCommitIndex, rf.commitIndex)
		for idx := oldCommitIndex + 1; idx < candidateCommitIndex+1; idx++ {
			committedMsg := new(ApplyMsg)
			committedMsg.Command = rf.log[idx].Command
			committedMsg.CommandIndex = idx
			committedMsg.CommandValid = true
			rf.applyCh <- *committedMsg
			//fmt.Printf("Leader %v commits message index %v with %v votes\n", rf.me, rf.commitIndex, count)
		}
	}
	rf.mu.Unlock()
	return ok
}

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {

	rf.mu.Lock()
	index := -1
	term := -1
	isLeader := (rf.currentState == "Leader")
	if isLeader == false {
		rf.mu.Unlock()
		return index, term, isLeader
	} else {
		log := new(LogEntry)
		log.Command = command
		log.Term = rf.currentTerm
		term = rf.currentTerm
		// change the log request ready state.
		rf.log = append(rf.log, log)
		index = len(rf.log) - 1
		//fmt.Printf("Someone called Start(), leader %v append log command %v in term %v\n", rf.me, log.Command, log.Term)
		rf.mu.Unlock()
	}
	//fmt.Printf("Start() successful\n")
	return index, term, isLeader
}

//
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

// <Leader resend appendEntries> <Candidate resend requestVote>in case of failure
func (rf *Raft) leader_resend(resend_chan chan bool) {
	for {
		if rf.killed() {
			return
		}
		rf.mu.RLock()
		if rf.currentState == "Leader" {
			rf.mu.RUnlock()
			time.Sleep(time.Duration(50) * time.Millisecond)
			rf.mu.RLock()
			if rf.currentState == "Leader" {
				rf.mu.RUnlock()
				resend_chan <- true
			} else {
				rf.mu.RUnlock()
				return
			}
		} else {
			rf.mu.RUnlock()
			return
		}
	}
}

func (rf *Raft) detectTimeout() {
	rf.mu.Lock()
	rf.currentState = "Follower"
	rand.Seed(time.Now().UnixNano())

	TimeOutValue := 300 + rand.Intn(300-150+1)
	timeoutchan := make(chan bool)
	sendVotechan := make(chan bool)

	rf.Args_out = make([]*RequestVoteReply, len(rf.peers))
	rf.Append_args_out = make([]*AppendEntriesReply, len(rf.peers))

	// initialized to be all 1s.
	rf.nextIndex = make([]int, len(rf.peers))
	// initialized to be me 0 0 0 0
	rf.matchIndex = make([]int, len(rf.peers))

	for idx := range rf.Args_out {
		// instantiate those motherfuckers.
		rf.Args_out[idx] = new(RequestVoteReply)
		rf.Append_args_out[idx] = new(AppendEntriesReply)
		rf.nextIndex[idx] = 1
		// instantiate the matchindex to be 0, since no log entry is replicated at any server now.
		rf.matchIndex[idx] = 0
	}
	rf.mu.Unlock()
	// < Candidate to Leader break>  <Follower resets timeout count break>
	go func() {
		// Polling the raft object resetTimeOut for changes.
		// This gorountine should NEVER exit.
		// there are two conditions we want to break the timeout counting and reset the timeout:
		// 1. when a HEARTBEAT has been received
		// 2. when a server is candidate and has received votes from majority of other servers
		for {
			for {
				if rf.killed() {
					return
				}
				rf.mu.RLock()
				if rf.currentState == "Follower" && rf.resetTimeOut == true {
					break
				}
				// Check number of responses with the same term.
				// Feed same information to reply.
				majority_number := len(rf.peers)/2 + 1
				count := 0
				for ch_idx := range rf.peers {
					if rf.currentTerm == rf.Args_out[ch_idx].Term && rf.Args_out[ch_idx].VoteGranted == true {
						count++
					}
				}
				if rf.currentState == "Candidate" && count >= majority_number {
					// break timeout if majority of votes have been received.
					break
				}
				rf.mu.RUnlock()

			}
			rf.mu.RUnlock()
			timeoutchan <- true
			rf.mu.Lock()
			rf.resetTimeOut = false
			rf.mu.Unlock()
		}
	}()

	for {

		TimedOut := false
		leaderResend := false
		select {
		// We assume the select statement hangs before one of timeoutchan and time.After() channels return.
		case <-timeoutchan:
			break
		case leaderResend = <-sendVotechan:
			break
		case <-time.After(time.Duration(TimeOutValue) * time.Millisecond):
			//fmt.Printf("Server %v timed out\n", rf.me)
			TimedOut = true
			break
		}
		if rf.killed() {
			return
		}

		rf.mu.RLock()
		leaderThisScope := rf.currentState == "Leader"
		// if rf.currentState != "Follower" {
		// 	fmt.Printf("Server %v current state is %v in term %v \n", rf.me, rf.currentState, rf.currentTerm)
		// }
		if rf.resetTimeOut == true {
			TimeOutValue = 300 + rand.Intn(300-150+1)
			rf.mu.RUnlock()
			continue
		}
		rf.mu.RUnlock()
		// Timeout only happens for follower and candidate
		if TimedOut == true && !leaderThisScope {
			rf.mu.Lock()
			//fmt.Printf("For server %d, current state is %s, in term %d\n", rf.me, rf.currentState, rf.currentTerm)
			if rf.currentState == "Follower" {
				rf.currentState = "Candidate"
				rf.currentTerm = rf.currentTerm + 1
				//fmt.Printf("Follower %d to candidate, current term is %d\n", rf.me, rf.currentTerm)
				// I also need to vote for myself.
				rf.votedFor = rf.me

				// Start election.
				rf.Args_out[rf.me].Term = rf.currentTerm
				rf.Args_out[rf.me].VoteGranted = true
				rf.votedFor = rf.me
				//fmt.Printf("Candidate %v started election in term %v\n", rf.me, rf.currentTerm)
				rf.mu.Unlock()
				for ch_idx := range rf.peers {
					if ch_idx != rf.me {
						// TODO: We are not doing logging yet, so we will fill all args_in[ch_idx] later.
						args := new(RequestVoteArgs)
						rf.mu.RLock()
						args.Term = rf.currentTerm
						args.CandidateId = rf.me
						args.LastLogIndex = len(rf.log) - 1
						if args.LastLogIndex >= 1 {
							args.LastLogTerm = rf.log[args.LastLogIndex].Term
						} else {
							args.LastLogTerm = -1
						}
						rf.mu.RUnlock()
						reply := new(RequestVoteReply)
						go rf.sendRequestVote(ch_idx, args, reply)
					}
				}
			} else if rf.currentState == "Candidate" {
				// Start re-election.
				// fmt.Printf("Candidate %d timeout, current term is %d\n", rf.me, rf.currentTerm)
				rf.currentTerm = rf.currentTerm + 1
				rf.Args_out[rf.me].Term = rf.currentTerm
				rf.Args_out[rf.me].VoteGranted = true
				rf.votedFor = rf.me
				//fmt.Printf("Candidate %v started re-election in term %v\n", rf.me, rf.currentTerm)
				rf.mu.Unlock()
				for ch_idx := range rf.peers {
					if ch_idx != rf.me {
						// feed information into args_in
						args := new(RequestVoteArgs)
						rf.mu.RLock()
						args.Term = rf.currentTerm
						args.CandidateId = rf.me
						args.LastLogIndex = len(rf.log) - 1
						if args.LastLogIndex >= 1 {
							args.LastLogTerm = rf.log[args.LastLogIndex].Term
						} else {
							args.LastLogTerm = -1
						}
						rf.mu.RUnlock()
						reply := new(RequestVoteReply)
						// We are not doing logging yet, so we will fill all args_in[ch_idx] later.
						go rf.sendRequestVote(ch_idx, args, reply)
					}
				}

			} else {
				rf.mu.Unlock()
			}

		} else {
			// 1. Follower correctly gets candidate's ping within the timeout value.
			// 2. Candidates correctly gets followers' ping within the timeout value.
			// Case 1 does not change server status, and we need to do NOTHING other than reseting the timeout value.
			// Case 2 requires me to reply to servers that sends me the agree message with the AppendRPC message.
			rf.mu.Lock()
			if rf.currentState == "Candidate" { // process is now candidate and have received major votes
				rf.currentState = "Leader"
				rf.Append_args_out[rf.me].Term = rf.currentTerm
				rf.Append_args_out[rf.me].Success = true
				rf.Append_args_out[rf.me].IsHeartbeat = false
				//fmt.Printf("Candidate %v became leaderin term %v\n", rf.me, rf.currentTerm)
				//start conuting timeout for leader resend
				for ch_idx := range rf.peers {
					if ch_idx != rf.me {
						rf.nextIndex[ch_idx] = len(rf.log)
					} else {
						rf.nextIndex[ch_idx] = 0
					}
					rf.matchIndex[ch_idx] = 0
				}
				rf.mu.Unlock()
				go rf.leader_resend(sendVotechan)
				for ch_idx := range rf.peers {
					rf.mu.RLock()
					append_args := new(AppendEntriesArgs)
					append_args.Term = rf.currentTerm
					append_args.PrevLogIndex = rf.nextIndex[ch_idx] - 1
					append_args.Type = "Heartbeat"
					append_args.LeaderId = rf.me
					if ch_idx != rf.me {
						append_reply := new(AppendEntriesReply)
						// Check whether the last log index >= next index for any of the servers.
						// If last log index < next index, it means that we only need to send them a heartbeat.
						//fmt.Printf("Just becomes a leader, log length is %v, next index for server %v is %v\n", len(rf.log), ch_idx, rf.nextIndex[ch_idx])
						if len(rf.log)-1 >= rf.nextIndex[ch_idx] {
							// log is waiting to be sent.
							append_args.LogEntries = make([]*LogEntry, 0)

							// Contains all new entries after the previous Log Index
							extraLogEntries := make([]*LogEntry, len(rf.log)-rf.nextIndex[ch_idx])
							copy(extraLogEntries, rf.log[rf.nextIndex[ch_idx]:len(rf.log)])
							append_args.LogEntries = extraLogEntries
							append_args.Type = "AppendEntries"
						}
						if append_args.PrevLogIndex >= 1 {
							append_args.PrevLogTerm = rf.log[append_args.PrevLogIndex].Term
						} else {
							append_args.PrevLogTerm = -1
						}
						//fmt.Printf("New LeaderCommit set to %v\n", append_args.LeaderCommit)
						append_args.LeaderCommit = rf.commitIndex
						if rf.currentState != "Leader" {
							rf.mu.RUnlock()
							break
						}
						rf.mu.RUnlock()
						go rf.sendAppendEntries(ch_idx, append_args, append_reply)
					} else {
						rf.mu.RUnlock()
					}

				}
			} else if rf.currentState == "Leader" {

				rf.Append_args_out[rf.me].Term = rf.currentTerm
				rf.Append_args_out[rf.me].Success = true
				rf.Append_args_out[rf.me].IsHeartbeat = false

				rf.mu.Unlock()
				if leaderResend == true {
					for ch_idx := range rf.peers {
						rf.mu.RLock()
						append_args := new(AppendEntriesArgs)
						append_args.Term = rf.currentTerm
						append_args.Type = "Heartbeat"
						append_args.PrevLogIndex = rf.nextIndex[ch_idx] - 1
						append_args.LeaderId = rf.me
						if ch_idx != rf.me {
							append_reply := new(AppendEntriesReply)
							if len(rf.log)-1 >= rf.nextIndex[ch_idx] {
								// log is waiting to be sent.
								append_args.LogEntries = make([]*LogEntry, 0)
								// Contains all new entries after the previous Log Index
								extraLogEntries := make([]*LogEntry, len(rf.log)-rf.nextIndex[ch_idx])
								copy(extraLogEntries, rf.log[rf.nextIndex[ch_idx]:len(rf.log)])
								append_args.LogEntries = extraLogEntries

								append_args.Type = "AppendEntries"
								//fmt.Printf("Server %v next index is %v\n", ch_idx, rf.nextIndex[ch_idx])
								//if len(append_args.LogEntries) > 0 {
								// fmt.Printf("Leader %v send AppendEntries to server %v with argsLog length %v, server log length %v vs %v\n", rf.me, ch_idx, len(append_args.LogEntries), len(rf.log), rf.nextIndex[ch_idx])
								// for index := range append_args.LogEntries {
								// 	fmt.Printf("%v ", append_args.LogEntries[index].Command)
								// }
								// fmt.Printf("\n")
								//}
							}
							if append_args.PrevLogIndex >= 1 {
								append_args.PrevLogTerm = rf.log[append_args.PrevLogIndex].Term
							} else {
								append_args.PrevLogTerm = -1
							}
							append_args.LeaderCommit = rf.commitIndex
							//fmt.Printf("New LeaderCommit set to %v\n", append_args.LeaderCommit)
							if rf.currentState != "Leader" {
								rf.mu.RUnlock()
								break
							}
							rf.mu.RUnlock()
							go rf.sendAppendEntries(ch_idx, append_args, append_reply)
						} else {
							rf.mu.RUnlock()
						}

					}
				}
			} else {
				rf.mu.Unlock()
			}

		}
		// handle case when the loop breaks -- The for loop will keep looping
		TimeOutValue = 300 + rand.Intn(300-150+1)
	}

}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.me = me
	rf.currentTerm = 0
	rf.votedFor = -1 // initialized to null value -1
	rf.currentState = "Follower"
	rf.log = make([]*LogEntry, 1)
	rf.commitIndex = 0
	rf.applyCh = applyCh
	// Your initialization code here (2A, 2B).
	go rf.detectTimeout()
	return rf
}
